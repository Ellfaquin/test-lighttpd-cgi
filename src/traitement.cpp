#include <iostream>
#include <string>
 
// Fonction pour obtenir la valeur du mot de passe
std::string getpassword(const std::string& text) {
    std::string password;
    size_t pos = text.find("password=");
    if (pos != std::string::npos) {
        pos += 9;  // Déplacer la position après "password="
        size_t end = text.find('&', pos); // Trouver le prochain '&'
        password = text.substr(pos, end - pos); // Extraire la valeur du mot de passe
    }
    return password;
}
 
// Fonction pour obtenir la valeur de l'utilisateur
std::string getuser(const std::string& text) {
    std::string user;
    size_t pos = text.find("user=");
    if (pos != std::string::npos) {
        pos += 5;  // Déplacer la position après "user="
        size_t end = text.find('&', pos); // Trouver le prochain '&'
        user = text.substr(pos, end - pos); // Extraire la valeur de l'utilisateur
    }
    return user;
}
 
int main() {
    // Récupérer les données envoyées par le formulaire
    std::string user, password, data;
    std::cin >> data;
    user = getuser(data);
    password = getpassword(data);
 
    // Afficher les données saisies
    std::cout << "Content-type:text/html\r\n\r\n";
    std::cout << "<html>\n";
    std::cout << "<head>\n";
    std::cout << "<title>Authentification - Traitement</title>\n";
    std::cout << "</head>\n";
    std::cout << "<body>\n";
    std::cout << "<h1>Authentification - Traitement</h1>\n";
    std::cout << "<p>Utilisateur: " << user << "</p>\n";
    std::cout << "<p>Mot de passe: " << password << "</p>\n";
    std::cout << "</body>\n";
    std::cout << "</html>\n";
 
    return 0;
}