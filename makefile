# Directories
SRCDIR = src
BINDIR = bin
HTMLDIR = pages
CONFDIR = conf
DESTDIR = /usr/webtest


# Files
SRCS = $(wildcard $(SRCDIR)/*.cpp)
CGIS = $(patsubst $(SRCDIR)/%.cpp, $(BINDIR)/%.cgi, $(SRCS))
CONFFILES = $(wildcard $(CONFDIR)/*)
PAGES = $(wildcard $(HTMLDIR)/*.html)

install: all
	install -d -m 0755 $(DEST)$(DESTDIR)/$(BINDIR)
	install -D -p -m 0755 $(CGIS) $(DEST)$(DESTDIR)/$(BINDIR)/
	install -d -m 0755 $(DEST)$(DESTDIR)/$(HTMLDIR)
	install -D -p -m 0755 $(PAGES) $(DEST)$(DESTDIR)/$(HTMLDIR)/
	install -d -m 0755 $(DEST)$(DESTDIR)/$(CONFDIR)
	install -D -p -m 0755 $(CONFFILES) $(DEST)$(DESTDIR)/$(CONFDIR)/

all: $(CGIS)

$(CGIS): $(SRCS)
	g++ $(CFLAGS) -o $@ $< $(LDFLAGS)

clean:
	rm -f $(CGIS)

.PHONY: all clean